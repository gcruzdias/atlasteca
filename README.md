Entrega do teste para Atlas. 
Tarefas realizadas em 23/09/2018 e 25/09/2018.

## Tarefas realizadas

### Ez Task's:

1. **Limitar registros, ações de editar e deletar filmes somente para administradores**
    - Criar uma regra pra diferenciar usuários normais de administradores

2. **Diferenciar os usuários** 
     - Esconder algumas informações dos filmes e mostrar somente se os usuários estiverem logados (Data de Lancamento e Sinopse)
     - Esconder informações de administradores dos usuários normais (Criar, Editar e Excluir filmes)

3. **Página Editar Filmes**  
     - Adicionar atributo duração do filme e mostrar na página para o usuário;

4. **Modificar Listagem**
     - Exibir um contador com todos filmes registrados no sistema
     - Utilizar paginação padrão do Laravel

### Normal Task's:

1. **Ordenar listagem**
     - Ordenar listagem padrão por data de lançamento do filme

2. **Criar página contato**
     - Criar uma página de contato e salvar os dados do formulário em banco de dados (criar tabela e página)

3. **Alterar estrutura do banco**  
     - Poder escolher mais de um gênero para cada filme
     - Quando deletar um filme não deletar ele do banco, somente desativar da listagem.

4. **Adicionar imagens nos filmes**
     - Criar um model de imagens e relacionar com o de filmes
     - Adicionar a possibilidade de administrador cadastrar a imagem nos filmes