<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMoviesGenders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('movies_genders', function(Blueprint $table)
      {
          $table->integer('movie_id')->unsigned()->nullable();
          $table->foreign('movie_id')->references('id')
                ->on('movies')->onDelete('cascade');

          $table->integer('gender_id')->unsigned()->nullable();
          $table->foreign('gender_id')->references('id')
                ->on('genders')->onDelete('cascade');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies_genders');
    }
}
