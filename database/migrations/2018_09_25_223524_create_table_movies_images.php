<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMoviesImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('movies_images', function(Blueprint $table)
      {
          $table->integer('movie_id')->unsigned()->nullable();
          $table->foreign('movie_id')->references('id')
                ->on('movies')->onDelete('cascade');

          $table->integer('image_id')->unsigned()->nullable();
          $table->foreign('image_id')->references('id')
                ->on('images')->onDelete('cascade');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies_images');
    }
}
