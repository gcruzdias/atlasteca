
<p align="center" style="max-width: 300px"><img src="http://atlastechnol.com/assets/images/logo.png"></p>

<br><br>


# Teste para avaliar conhecimento

Não é obrigatório realizar todas as tarefas. 
Iremos considerar tudo que for enviado!

**Lembrem-se**, utilizamos como padrão toda a documentação oficial do Laravel, ([Laravel Docs](https://laravel.com/docs/5.2)).

## Tarefas 

### Ez Task's:

1. **Limitar registros, ações de editar e deletar filmes somente para administradores**
    - Criar uma regra pra diferenciar usuários normais de administradores

2. **Diferenciar os usuários** 
     - Esconder algumas informações dos filmes e mostrar somente se os usuários estiverem logados (Data de Lancamento e Sinopse)
     - Esconder informações de administradores dos usuários normais (Criar, Editar e Excluir filmes)

3. **Página Editar Filmes**  
     - Adicionar atributo duração do filme e mostrar na página para o usuário;

4. **Modificar Listagem**
     - Exibir um contador com todos filmes registrados no sistema
     - Utilizar paginação padrão do Laravel

### Normal Task's:

1. **Ordenar listagem**
     - Ordenar listagem padrão por data de lançamento do filme

2. **Criar página contato**
     - Criar uma página de contato e salvar os dados do formulário em banco de dados (criar tabela e página)

3. **Alterar estrutura do banco**  
     - Poder escolher mais de um gênero para cada filme
     - Quando deletar um filme não deletar ele do banco, somente desativar da listagem.

4. **Adicionar imagens nos filmes**
     - Criar um model de imagens e relacionar com o de filmes
     - Adicionar a possibilidade de administrador cadastrar a imagem nos filmes

### Premium Task's

1. **Adicionar sistema de doação**
     - Criar um modal pedindo doações com 2 botões (Doar, Cancelar), sempre que usuário cadastrado navegar
     - Se clicar em doar ir para uma página de doação pedindo os dados e salvando eles em uma tabela no banco de dados (Criar tabela e página)
     - Se clicar em cancelar, fechar o modal e não aparecer mais no site até ele fechar a janela
     - Se usuário realizar doação, não mostrar modal de doação durante um mês

2. **Adicionar sistema de popularidade**
     - Criar um sistema de likes e dislikes nos filmes com um sistema de popularidade.
     - Cada usuário deve conseguir dar somente um like ou dislike por filme (não precisa estar logado). Dica: fingerprint valve
     - Utilizar sistema de cache para salvar os likes. Dica: redis
     - Poder ordenar a listagem principal por popularidade removendo a listagem padrão