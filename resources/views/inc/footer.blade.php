<!-- JavaScripts -->
<script src="{{ asset('js/all.js') }}"></script>
@if(Auth::check())
	@if(!Route::is('donation.store'))
		@if(!Route::is('donation.create'))
			@if(!Route::is('donation.cancel'))
				@if(!Session::has('donationShowed')))
					<!-- Modal -->
					<div class="modal fade" id="donation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="myModalLabel">Faça uma doação!</h4>
					            </div>
					            <div class="modal-body text-center">
					            	Faça uma doação para a Atlasteca e ajude-nos a mantê-la no ar.
					            </div>
					            <div class="modal-footer">
					                <a href="{{ url('/donation/make') }}""><button type="button" class="btn btn-info">Doar</button></a>
					                <a href="{{ url('/donation/dismiss') }}"><button type="button" class="btn btn-default">Cancelar</button></a>
					            </div>
					        </div>
					    </div>
					</div>
					<!-- JavaScripts -->
					<script src="{{ asset('js/modal-donation.js') }}"></script>
				@endif
			@endif
		@endif
	@endif
@endif