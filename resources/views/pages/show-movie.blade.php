@extends('layouts.app')

@section('content')

    <div class="container lists">

        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="{{ asset('images/logo.jpg') }}" style="max-width: 100%; width: 500px;">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 single-movie">
                <h1>
                    {{ $movie->title }}
                    @if (Auth::check())
                        @if($movie->year)
                            <small> ({{ $movie->year }}) </small>
                        @endif
                    @endif
                </h1>
                @if ($movie->images()->first()['path'] != '')
                    <div class="row">
                        @foreach($movie->images as $image)
                            <div class="col-xs-3 single-movie">
                                <img src="{{ asset('images/'.$image->path) }}" alt="{{ $movie->title }}" class="pull-left img-responsive cover img-thumbnail margin10" data-toggle="modal" data-target="#myModal">
                            </div>
                        @endforeach
                    </div>
                @else
                    <img class="cover" src="{{ asset('images/noimage.jpg') }}" alt="{{ $movie->title }}">
                @endif
                <article>
                    <p>
                        @if (Auth::check())
                            {{ $movie->description }}
                        <br>
                        @endif
                        
                        <br>
                        <span> Genêros: 
                        @foreach($movie->genders as $gender)
                            {{ $gender->title }}
                        @endforeach
                        </span>

                        <br>
                        <span> Duração: {{ $movie->duration }}</span>
                    </p>
                </article>
            </div>
        </div>

    </div>

    {{--MODAL--}}

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        {{ $movie->title }}
                        @if($movie->year)
                            <small> ({{ $movie->year }}) </small>
                        @endif
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <img src="{{ asset('images/noimage.jpg') }}" alt="{{ $movie->title }}" class="img-responsive displayinlineblock">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
