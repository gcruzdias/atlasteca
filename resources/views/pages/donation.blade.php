@extends('layouts.app')

@section('content')

    <div class="container">

        <h2>Doação</h2>

        <form action="{{ route('donation.store') }}" method="post">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="value">Valor</label>
                            <select name="value" id="value" class="form-control">
                                <option value="5">R$5,00</option>
                                <option value="10">R$10,00</option>
                                <option value="15">R$15,00</option>
                                <option value="20">R$20,00</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <button class="btn btn-primary">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i>Realizar doação</button>
                    <a href="{{ route('user.dashboard') }}" class="btn btn-default">
                        Voltar
                    </a>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('after_footer')

@endsection