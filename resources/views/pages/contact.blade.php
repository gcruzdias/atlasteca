@extends('layouts.app')

@section('content')

    <div class="container">

        <h2>Contato</h2>

        <form action="{{ route('contact.store') }}" method="post">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input name="name" id="name" type='text' class="form-control" placeholder="Seu nome">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input name="email" id="email" type='email' class="form-control" placeholder="Seu email">
                    </div>
                    <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea name="message" id="message" class="form-control" rows="3" placeholder="Insira sua mensagem"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <button class="btn btn-primary">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i>Salvar</button>
                    <a href="{{ route('user.dashboard') }}" class="btn btn-default">
                        Voltar
                    </a>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('after_footer')

@endsection