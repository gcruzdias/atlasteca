<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function genders()
    {
        return $this->belongsToMany('App\Gender','movies_genders','movie_id','gender_id');
    }
    
    public function images() {
        return $this->hasMany('App\Image');
    }
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
