<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('tt', ['as' => 'tt', function(){

    $user = \App\User::first();
    $gender = \App\Gender::first();
    $movie = \App\Movie::first();

    $movie->load('gender');

    dump($user);
    dump($gender);
    dump($movie);

}]);

Route::auth();

Route::get('/',
           [
               'as' => 'home',
               'uses' => 'AppController@lists'
           ]
);

Route::get('/dashboard',
           [
               'as' => 'user.dashboard',
               'uses' => 'UserController@index',
           ]
);

Route::get('/movie/create',
           [
               'as' => 'movie.create',
               'uses' => 'MovieController@create',
               'middleware' => 'admin'
           ]
);

Route::post('/movie/create',
           [
               'as' => 'movie.store',
               'uses' => 'MovieController@store',
               'middleware' => 'admin'
           ]
);

Route::get('/movie/edit/{id}',
           [
               'as' => 'movie.edit',
               'uses' => 'MovieController@edit',
               'middleware' => 'admin'
           ]
);

Route::post('/movie/edit/{id}',
           [
               'as' => 'movie.update',
               'uses' => 'MovieController@update',
               'middleware' => 'admin'
           ]
);

Route::get('/movie/delete/{id}',
           [
               'as' => 'movie.delete',
               'uses' => 'MovieController@delete',
               'middleware' => 'admin'
           ]
);

Route::get('movie/{id}/{slug?}',
           [
               'as' => 'movie.show',
               'uses' => 'MovieController@show'
           ]
);

Route::get('/contact',
           [
               'as' => 'contact.create',
               'uses' => 'ContactController@create'
           ]
);

Route::post('/contact',
           [
               'as' => 'contact.store',
               'uses' => 'ContactController@store'
           ]
);

Route::get('/donation/make',
           [
               'as' => 'donation.create',
               'uses' => 'DonationController@create'
           ]
);

Route::post('/donation/make',
           [
               'as' => 'donation.store',
               'uses' => 'DonationController@store'
           ]
);

Route::get('/donation/dismiss',
           [
               'as' => 'donation.cancel',
               'uses' => 'DonationController@cancel'
           ]
);

Route::get('/logout',
           [
               'as' => 'user.logout',
               'uses' => 'UserController@logout'
           ]
);
