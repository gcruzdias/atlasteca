<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function create(){
        return view('pages.contact');
    }

    public function store(){
        $inputs = $this->request->all();
        $this->validate($this->request, [
            'name' => 'required|max:255',
            'email' => 'email|required',
            'message' => 'required'
        ]);
        $contact = New Contact();
        $contact->name = $inputs['name'];
        $contact->email = $inputs['email'];
        $contact->message = $inputs['message'];
        $contact->save();

        return redirect()
            ->route('home')
            ->with([
                       'success' => 'Contato enviado com sucesso!',
                   ]);
    }

}
