<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Donation;

class DonationController extends Controller
{
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function create(){
        return view('pages.donation');
    }

    public function cancel(){
        $this->request->session()->put('donationShowed', 1);
        return redirect()
            ->route('home')
            ->with([
                       'warning' => 'Sem problema! Você será convidado a doar na próxima vez que acessar nosso site.',
                   ]);
    }

    public function store(){

        $inputs = $this->request->all();

        $this->validate($this->request, [
            'value' => 'required'
        ]);

        $donation = New Donation();
        $donation->user_id = \Auth::user()->id;
        $donation->value = $inputs['value'];
        $donation->save();

        $this->request->session()->put('donationShowed', 1);
        return redirect()
            ->route('user.dashboard')
            ->with([
                       'success' => 'Doação realizada com sucesso!',
                   ]);
    }

}
