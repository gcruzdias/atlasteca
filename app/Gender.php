<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{

    public function movies()
    {
        return $this->belongsToMany('App\Movie','movies_genders','gender_id','movie_id');
    }
}
